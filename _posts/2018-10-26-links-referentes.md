---
layout: post
categories: posts
title: "Algunos links de referencias para ver y analizar"
---


# Algunos links de referencias para ver y analizar
## Textos, artistas visuales y/o sonoros

* https://icasea.bandcamp.com/album/waiting-for-junior /
* https://computerclub.bandcamp.com/album/peak-cut / todo lo de computer club / hubo una fecha de yaxu + mark fell (el de SND) + cardenas
* https://66.media.tumblr.com/82e71b64c0821a1323fedf2305155165/tumblr_oa49dgdm3Y1s77r3ao1_1280.jpg
* https://pad.riseup.net/p/discos-love-listen /
* https://www.youtube.com/watch?v=tVg7mPo-Qvs / Tujiko Noriko
* https://www.youtube.com/watch?v=Zrn1K7GKUDw / Aoki + Tujiko
* https://www.youtube.com/watch?v=Pv-2PqiqHKc / Aoki
* https://pitchfork.com/features/lists-and-guides/10011-the-50-best-idm-albums-of-all-time/?page=5 / 50 "mejores" discos de IDM escrita por Reynolds
* https://www.youtube.com/watch?v=BH3AC2GcOhE / SND
* http://www.fennesz.com/ / Fennez
* https://oval.bandcamp.com/album/94diskont-remastered-2013-reissue / Oval
* https://www.youtube.com/watch?v=g9Z6CWuxHZU / Ryoji Ikeda
* https://www.felipepantone.com/  / Felipe Pantone, artista visual que elabora desde una estética digital murales, pinturas, instalaciones, hechas a mano y otras veces impresas
* http://www.peterburr.org/
* http://www.lukelab.com/
* http://straylandings.co.uk/interviews/hit-enter-renick-bell
* https://pousta.com/rosa-menkman-te-explica-de-pies-a-cabeza-el-arte-del-error/  ///artículo sobre el Glitch
* http://www.smoothware.com/danny/ / artista que trabaja creando imágenes interactivas que responden a modo de reflejo de los movimientos del público
* https://vimeo.com/260741794 / Olafur Eliasson, otra forma de pensar para intervenir con luz, a traves de diferentes materiales que pueden modificar proyecciones por ejemplo
* http://www.cruz-diez.com/es/work/chromointerference/2010-to-date/  ///artista que hace arte óptico, sus últimas muestras fueron realizando proyecciones en varias paredes
* https://nextshark.com/juha-van-ingen-janne-sarkela-longest-gif/  /// un GIF que dura 1000 años, basado en John Cage
* http://selfiecity.net/  /// la complejidad de internet, la selfie y los datos
* https://www.nonotak.com/
* https://www.youtube.com/watch?v=Je0VGmzY_R4&feature=youtu.be /// Entrevista al filósofo Franco Berardi
* https://elpais.com/cultura/2018/10/15/actualidad/1539616017_797032.html?id_externo_rsoc=FB_CC&fbclid=IwAR3Gur_y28bLF8RNJoXdktUYo0iJzzwEGxhFcuH7J3DFOG56r5I19DYpPSg  ///Entrevista a Luis Camnitzer
* http://lobosuelto.com/?p=21922  ///Entrevista a Bifo
